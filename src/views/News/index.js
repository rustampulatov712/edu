import React from 'react';
import style from './index.module.scss'
import img1 from '../../assets/news/imgs.png'
import img2 from '../../assets/news/pupil.png'
import img3 from '../../assets/news/globs.png'
import img4 from '../../assets/news/teacher.png'
import {ReactComponent as Svg} from "../../assets/img/long_up_right.svg";

const Index = () => {
    return (
        <div className={'cardNews'}>
            <div className={'cardImg'}>
                <div className={`${style.wrapper} container`}>
                    <span className={style.wrapperTitle}>News</span>
                    <h1>Stay up to date with our news</h1>
                    <div className={style.wrapperCard}>
                        <div className={style.wrapperCards}>
                            <img src={img1} alt="noImg"/>
                            <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                            <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                            <div className={style.wrapperCardFooter}>
                                <button className={style.wrapperCardsLeft}>
                                    Learn More
                                    <Svg className={style.nextPa}/>
                                </button>

                                <span>18.01.2022</span>
                            </div>

                        </div>
                        <div className={style.wrapperCards}>
                            <img src={img2} alt="noImg"/>
                            <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                            <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                            <div className={style.wrapperCardFooter}>
                                <button className={style.wrapperCardsLeft}>
                                    Learn More
                                    <Svg className={style.nextPa}/>
                                </button>

                                <span>18.01.2022</span>
                            </div>

                        </div>
                        <div className={style.wrapperCards}>
                            <img src={img3} alt="noImg"/>
                            <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                            <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                            <div className={style.wrapperCardFooter}>
                                <button className={style.wrapperCardsLeft}>
                                    Learn More
                                    <Svg className={style.nextPa}/>
                                </button>

                                <span>18.01.2022</span>
                            </div>

                        </div>
                        <div className={style.wrapperCards}>
                            <img src={img4} alt="noImg"/>
                            <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                            <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                            <div className={style.wrapperCardFooter}>
                                <button className={style.wrapperCardsLeft}>
                                    Learn More
                                    <Svg className={style.nextPa}/>
                                </button>

                                <span>18.01.2022</span>
                            </div>

                        </div>
                    </div>
                </div>



            </div>

        </div>
    );
};

export default Index;