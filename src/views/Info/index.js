import React from 'react';
import style from './index.module.scss'
import {ReactComponent as ActiveSvg} from '../../assets/results/next.svg';
import Test from "../test";

const Index = () => {

    return (
        <div className={'cardNews'}>
            <div className={`${style.wrapperCard} container`}>
                <div className={style.headerText}>
                    <span className={style.htextArea}>
                        Branches
                    </span>
                    <h3>
                        Information about our branches
                    </h3>
                </div>
                <Test/>
                <div className={style.nextPage}>
                    <ActiveSvg className={style.nextPageLeft}/>
                    <ActiveSvg className={style.nextPageRinght}/>

                </div>

            </div>
        </div>);
};

export default Index;