import React from 'react';
import style from './test.module.scss'
import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination} from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import img from "../../assets/news/globs.png";
import vector from "../../assets/info/Vector.svg";
import location from "../../assets/info/location.svg";
import clock from "../../assets/img/soat.svg";

const Index = () => {

    return (
        <div className={style.cdd}>
            <Swiper navigation={false} modules={[Navigation]} className="mySwiper"
            >
                <div>
                    <SwiperSlide><div className={style.cards}>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>Chust filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>To’raqo’rg’on filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                    </div></SwiperSlide>
                    <SwiperSlide><div className={style.cards}>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>Chust filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>To’raqo’rg’on filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                    </div></SwiperSlide>
                    <SwiperSlide><div className={style.cards}>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>Chust filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>To’raqo’rg’on filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                    </div></SwiperSlide>
                    <SwiperSlide><div className={style.cards}>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>Chust filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                        <div className={style.wrapperBox}>
                            <div className={style.cardBodyLeft}>
                                <img src={img} alt="noImg"/>
                            </div>
                            <div className={style.cardBodyRing}>
                                <h6>To’raqo’rg’on filiali</h6>
                                <div className={style.cardbodyText}>
                                    <img src={vector} alt=""/>
                                    <span className={style.cardheaderText}>Namangan vil., Chust tumani, Sharof Rashidov ko‘chasi 5A</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={location} alt=""/>
                                    <span className={style.cardSecondText}><span>Orienter: </span>
                                Chust istirohat bog‘i yaqinida</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={clock} alt=""/>
                                    <span className={style.cardSecondText}><span>Ish vaqti:</span>
                                09:00 - 20:00</span>
                                </div>
                            </div>
                        </div>
                    </div></SwiperSlide>

                </div>


            </Swiper>
        </div>
    );
};

export default Index;