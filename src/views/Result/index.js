import React from 'react';
import style from './result.module.scss'
import userInfo from '../../assets/results/Ellipse 20.svg'
import infoImg from '../../assets/results/Group 19.svg'
import {ReactComponent as ActiveSvg} from '../../assets/results/next.svg';
import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination} from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";


const Index = () => {
    return (
        <div className='cardResults'>
            <div className={`${style.wrapper} container`}>
                <span className={style.wrappersubTitle}>Our rusults</span>
                <h1>Our students won GRANTS to
                    <br/>
                    TOP universities in the world.</h1>
                <Swiper navigation={false} modules={[Navigation]} className="mySwiper" onChange={(e)=> console.log(e.target.value)}>
                    <SwiperSlide>
                        <div className={style.wrapperCards}>
                            <div className={style.wrapperCard}>
                                <img src={infoImg} alt="no img" className={style.cardImg}/>
                                <div className={style.wrapperCardText}>
                                    <div className={style.userInfo}>
                                        <div>
                                            <img src={userInfo} alt="no img"/>
                                        </div>
                                        <div className={style.userInfoName}>
                                            <h6>Sarvar Erkinjonov</h6>
                                            <p>O’qituvchisi: <b>Elyor Mirzayev</b></p>
                                        </div>
                                    </div>
                                    <div className={style.userBalls}>
                                        <span className={style.userBallsText}>Listening:<b>9.0</b></span>
                                        <span className={style.userBallsText}>Reading:<b>8.5</b></span>
                                        <span className={style.userBallsText}>Writing:<b>7.5</b></span>
                                        <span className={style.userBallsText}>Speaking:<b>7.0</b></span>
                                        <h6>Band Score:<b>8.0</b></h6>
                                    </div>
                                </div>

                            </div>
                            <div className={style.wrapperCard}>
                                <img src={infoImg} alt="no img" className={style.cardImg}/>
                                <div className={style.wrapperCardText}>
                                    <div className={style.userInfo}>
                                        <div>
                                            <img src={userInfo} alt="no img"/>
                                        </div>
                                        <div className={style.userInfoName}>
                                            <h6>Sarvar Erkinjonov</h6>
                                            <p>O’qituvchisi: <b>Elyor Mirzayev</b></p>
                                        </div>
                                    </div>
                                    <div className={style.userBalls}>
                                        <span className={style.userBallsText}>Listening:<b>9.0</b></span>
                                        <span className={style.userBallsText}>Reading:<b>8.5</b></span>
                                        <span className={style.userBallsText}>Writing:<b>7.5</b></span>
                                        <span className={style.userBallsText}>Speaking:<b>7.0</b></span>
                                        <h6>Band Score:<b>8.0</b></h6>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={style.wrapperCards}>
                            <div className={style.wrapperCard}>
                                <img src={infoImg} alt="no img" className={style.cardImg}/>
                                <div className={style.wrapperCardText}>
                                    <div className={style.userInfo}>
                                        <div>
                                            <img src={userInfo} alt="no img"/>
                                        </div>
                                        <div className={style.userInfoName}>
                                            <h6>Sarvar Erkinjonov</h6>
                                            <p>O’qituvchisi: <b>Elyor Mirzayev</b></p>
                                        </div>
                                    </div>
                                    <div className={style.userBalls}>
                                        <span className={style.userBallsText}>Listening:<b>9.0</b></span>
                                        <span className={style.userBallsText}>Reading:<b>8.5</b></span>
                                        <span className={style.userBallsText}>Writing:<b>7.5</b></span>
                                        <span className={style.userBallsText}>Speaking:<b>7.0</b></span>
                                        <h6>Band Score:<b>8.0</b></h6>
                                    </div>
                                </div>

                            </div>
                            <div className={style.wrapperCard}>
                                <img src={infoImg} alt="no img" className={style.cardImg}/>
                                <div className={style.wrapperCardText}>
                                    <div className={style.userInfo}>
                                        <div>
                                            <img src={userInfo} alt="no img"/>
                                        </div>
                                        <div className={style.userInfoName}>
                                            <h6>Sarvar Erkinjonov</h6>
                                            <p>O’qituvchisi: <b>Elyor Mirzayev</b></p>
                                        </div>
                                    </div>
                                    <div className={style.userBalls}>
                                        <span className={style.userBallsText}>Listening:<b>9.0</b></span>
                                        <span className={style.userBallsText}>Reading:<b>8.5</b></span>
                                        <span className={style.userBallsText}>Writing:<b>7.5</b></span>
                                        <span className={style.userBallsText}>Speaking:<b>7.0</b></span>
                                        <h6>Band Score:<b>8.0</b></h6>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={style.wrapperCards}>
                            <div className={style.wrapperCard}>
                                <img src={infoImg} alt="no img" className={style.cardImg}/>
                                <div className={style.wrapperCardText}>
                                    <div className={style.userInfo}>
                                        <div>
                                            <img src={userInfo} alt="no img"/>
                                        </div>
                                        <div className={style.userInfoName}>
                                            <h6>Sarvar Erkinjonov</h6>
                                            <p>O’qituvchisi: <b>Elyor Mirzayev</b></p>
                                        </div>
                                    </div>
                                    <div className={style.userBalls}>
                                        <span className={style.userBallsText}>Listening:<b>9.0</b></span>
                                        <span className={style.userBallsText}>Reading:<b>8.5</b></span>
                                        <span className={style.userBallsText}>Writing:<b>7.5</b></span>
                                        <span className={style.userBallsText}>Speaking:<b>7.0</b></span>
                                        <h6>Band Score:<b>8.0</b></h6>
                                    </div>
                                </div>

                            </div>
                            <div className={style.wrapperCard}>
                                <img src={infoImg} alt="no img" className={style.cardImg}/>
                                <div className={style.wrapperCardText}>
                                    <div className={style.userInfo}>
                                        <div>
                                            <img src={userInfo} alt="no img"/>
                                        </div>
                                        <div className={style.userInfoName}>
                                            <h6>Sarvar Erkinjonov</h6>
                                            <p>O’qituvchisi: <b>Elyor Mirzayev</b></p>
                                        </div>
                                    </div>
                                    <div className={style.userBalls}>
                                        <span className={style.userBallsText}>Listening:<b>9.0</b></span>
                                        <span className={style.userBallsText}>Reading:<b>8.5</b></span>
                                        <span className={style.userBallsText}>Writing:<b>7.5</b></span>
                                        <span className={style.userBallsText}>Speaking:<b>7.0</b></span>
                                        <h6>Band Score:<b>8.0</b></h6>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </SwiperSlide>

                </Swiper>

                <div className={style.nextPage}>
                    <ActiveSvg className={style.nextPageLeft}/>
                    <ActiveSvg className={style.nextPageRinght}/>

                </div>


            </div>
        </div>
    );
};

export default Index;