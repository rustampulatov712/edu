import React from 'react';
import style from "./index.module.scss";
import {ReactComponent as Svg} from "../../assets/img/long_up_right.svg";
import img1 from "../../assets/blog/kallega.png";
import img2 from "../../assets/blog/homeworking.png";
import img4 from "../../assets/blog/lessonRoom.png";
import img3 from "../../assets/blog/studensts.png";

const Index = () => {
    return (
        <div className={'cardNews'}>
            <div className={`${style.wrappers} container`}>
                <span className={style.wrapperTitle}>Blogs</span>
                <h1>Our blogs</h1>
                <div className={style.wrapperCardd}>
                    <div className={style.wrapperCards}>
                        <div className={style.wrapperCardsImg}>
                            <img src={img1} alt=""/>
                        </div>
                        <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                        <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                        <div className={style.wrapperCardFooter}>
                            <button className={style.wrapperCardsLeft}>
                                Learn More
                                <Svg className={style.nextPa}/>
                            </button>

                            <span>18.01.2022</span>
                        </div>

                    </div>
                    <div className={style.wrapperCards}>
                        <div className={style.wrapperCardsImg}>
                            <img src={img2} alt=""/>
                        </div>
                        <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                        <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                        <div className={style.wrapperCardFooter}>
                            <button className={style.wrapperCardsLeft}>
                                Learn More
                                <Svg className={style.nextPa}/>
                            </button>

                            <span>18.01.2022</span>
                        </div>

                    </div>
                    <div className={style.wrapperCards}>
                        <div className={style.wrapperCardsImg}>
                            <img src={img3} alt=""/>
                        </div>
                        <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                        <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                        <div className={style.wrapperCardFooter}>
                            <button className={style.wrapperCardsLeft}>
                                Learn More
                                <Svg className={style.nextPa}/>
                            </button>

                            <span>18.01.2022</span>
                        </div>

                    </div>
                    <div className={style.wrapperCards}>
                        <div className={style.wrapperCardsImg}>
                            <img src={img4} alt=""/>
                        </div>
                        <h3>Amet minim mollit non deserunt ullamco est sit .</h3>
                        <span>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. </span>
                        <div className={style.wrapperCardFooter}>
                            <button className={style.wrapperCardsLeft}>
                                Learn More
                                <Svg className={style.nextPa}/>
                            </button>

                            <span>18.01.2022</span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    );
};

export default Index;