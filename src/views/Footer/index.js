import React from 'react';
import style from "./index.module.scss"
import insta from '../../assets/footer/instagram.svg'
import facebook from '../../assets/footer/facebook.svg'
import youtobe from '../../assets/footer/youtube.svg'
import telegram from '../../assets/footer/telegram.svg'
import logo from '../../assets/img/Logo.svg'

const Index = () => {
    return (
        <div className={'cardFooter'}>
            <div className={`${style.wrapper} container`}>
                <div className={style.wrapperText}>

                    <img src={logo} alt="logo"/>
                    <div className={style.wrapperItem}>
                            <span className={style.textLorem}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci explicabo ipsa nisi obcaecati perferendis perspiciatis quis reprehenderit! Atque, dolore temporibus!
                        </span>
                        <span className={style.textNomer}>+998 99 033 73 67</span>
                        <span className={style.textAddres}>Namangan, st. Islom Karimov, 16A</span>
                        <span className={style.textInfo}>info@spceducation.uz</span>
                    </div>

                    <div className={style.web}>
                        <a href="https://www.instagram.com/"> <img src={insta} alt=""/></a>
                        <a href="https://ru-ru.facebook.com/"> <img src={facebook} alt=""/></a>
                        <a href="https://www.youtube.com/"> <img src={youtobe} alt=""/></a>
                        <a href="https://telegram.org/"> <img src={telegram} alt=""/></a>

                    </div>

                </div>
                <div className={style.loactionCard}>
                    <span>Location</span>
                    <div className={style.loactioncard}>
                        <iframe
                            src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa976e2f49cc32244cee130c971caaeac5bbec1bbe77d1485fec2a3a4cb3dbefa&amp;source=constructor"
                            width="400" height="240" frameBorder="0"></iframe>
                        {/*<img src={loaction} alt=""/>*/}
                    </div>
                    <div className={style.yearText}>
                        <p>© 2000-2021, All Rights Reserved</p>
                    </div>
                </div>
            </div>
            <script src="//code.jivosite.com/widget/v8NDz3sg52" async></script>
        </div>
    );
};

export default Index;