import React from 'react';
import style from "./index.module.scss"

const Index = () => {
    return (
        <div className={'cardSPC'}>
            <div className={`${style.wrapper} container`}>
                <div className={style.headerText}>
                    <span className={style.htextArea}>
                        SPC life
                    </span>
                    <h3>
                        About us. SPC life events
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim volutpat amet senectus erat
                        ultrices eu. Ultrices condimentum volutpat amet maecenas dictum sit tellus porttitor dictumst.
                    </p>
                </div>
                <div className={style.wrapperCard}>
                    <div className={style.wrapperCardItems}>
                        <img src="https://shosh.uz/wp-content/uploads/2016/05/Tashkent-Toshkent-10.jpg" alt="noImg"/>
                    </div>
                    <div className={style.wrapperCardItems}>
                        <img src="https://shosh.uz/wp-content/uploads/2016/05/Tashkent-Toshkent-10.jpg" alt="noImg"/>
                    </div>
                    <div className={style.wrapperCardItems}>
                        <img src="https://shosh.uz/wp-content/uploads/2016/05/Tashkent-Toshkent-10.jpg" alt="noImg"/>
                    </div>
                    <div className={style.wrapperCardItems}>
                        <img src="https://shosh.uz/wp-content/uploads/2016/05/Tashkent-Toshkent-10.jpg" alt="noImg"/>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default Index;