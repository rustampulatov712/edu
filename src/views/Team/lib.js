import Safiya from '../../assets/team/Safiya.svg'
import Katya from '../../assets/team/Katya.svg'
import Maria from '../../assets/team/Mariya.svg'
import Malina from '../../assets/team/Malina.svg'

export const data = [
    {
        id: 1,
        img: Safiya,
        name: 'Safia Abdullayeva',
        role: 'Ingliz tili o’qituvchisi',
        workDuration: '2.5'
    },
    {
        id: 2,
        img: Malina,
        name: 'Malina Abdullayeva',
        role: 'Ingliz tili o’qituvchisi',
        workDuration: '2.5'
    },
    {
        id: 3,
        img: Katya,
        name: 'Katya Abdullayeva',
        role: 'Ingliz tili o’qituvchisi',
        workDuration: '2.5'
    },

    {
        id: 3,
        img: Maria,
        name: 'Maria Abdullayeva',
        role: 'Ingliz tili o’qituvchisi',
        workDuration: '2.5'
    },
]