import React from 'react';
import style from './team.module.scss'
import job from '../../assets/team/Work.svg'
import {data} from './lib'

const Index = () => {
    return (
        <div className='cardTaem'>
            <div className={`${style.wrapper} container`}>
                <div className={style.headerTitle}>
                    <span>
                        Our team
                    </span>
                    <h3>Our qualified specialists</h3>
                </div>
                <div className={style.wrapperCard}>
                    {
                        data.map((item) => {
                            return (
                                <div className={style.cardBody}>
                                    <img src={item.img} alt=""/>
                                    <div className={style.cardBodyTitle}>
                                        <div className={style.cardTitle}>
                                            <h4>{item.name}</h4>
                                            <span>{item.role}</span>
                                            <div className={style.cardFooterTitle}>
                                                <img src={job} alt="no"/>
                                                <span>Tajriba: <b>{item.workDuration} yil</b></span>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            )

                        })
                    }
                </div>


            </div>
        </div>
    );
};

export default Index;