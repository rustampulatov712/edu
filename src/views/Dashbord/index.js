import React from 'react';
import style from './index.module.scss'
import logo from '../../assets/img/Logo.svg'
import ava from '../../assets/img/personAva1.png'
import str from '../../assets/img/long_up_right.svg'
import bars from "../../assets/bars-solid.svg"
import close from "../../assets/close.png"

const Index = () => {

    const [open, setOpen] = React.useState(false);
    const openNavbar = () => {
        setOpen(!open);
    }
    return (
        <div className=' cardDash'>
            <div className={`${style.dashbord} container`}>
                <div className={style.dashbordNav}>
                    <img src={logo} alt="logo"/>

                    <ul>
                        <div className={style.drop}>
                            <span>O‘zbekcha</span>
                        </div>

                        <li>Kurslar</li>
                        <li>Jamoa</li>
                        <li>Natijalar</li>
                        <li>Yangiliklar</li>
                        <li>Blog</li>
                        <li>SPC hayoti</li>

                    </ul>
                    <div className={`${style.mobileNav}`} onClick={openNavbar}>
                        {open ? <img src={close} alt=""/> : <img src={bars} alt="no"/>}
                    </div>
                    <div className={`${open && style.mobileNavItemClose} ${style.mobileNavItem}`}>
                        {/*{!open &&/*/}
                            <div className={style.navUl}>
                                <div className={style.navLanguage}>
                                    <span>O‘zbekcha</span>
                                </div>
                                <ul>
                                    <li>Kurslar</li>
                                    <li>Jamoa</li>
                                    <li>Natijalar</li>
                                    <li>Yangiliklar</li>
                                    <li>Blog</li>
                                    <li>SPC hayoti</li>
                                </ul>
                            </div>
                        {/*// }*/}
                    </div>

                </div>
                <div className={style.dashbordBody}>
                    <div className={style.dashbordBodyLeft}>
                        <span>Experienced education centre</span>
                        <h3>Let’s learn about new
                            knowledge and abilities.</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim volutpat amet senectus erat
                            ultrices eu. Ultrices condimentum volutpat amet maecenas dictum sit tellus porttitor
                            dictumst. </p>
                        <button>
                            Kursga yozilish
                            <img src={str} alt=""/>
                        </button>
                    </div>
                    <div className={style.dashbordBodyRinght}>
                        <img src={ava} alt="noImg"/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Index;