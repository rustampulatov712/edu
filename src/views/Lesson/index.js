import React, {useState} from 'react';
import style from "./index.module.scss";

import str from "../../assets/img/long_up_right.svg";
import {data} from './libs'

const Index = () => {
    const [open, setOpen] = useState(1)
    const clicked = (number) => {
        setOpen(number)
    }


    return (
        <div className='cardLesson'>
            <div className={`${style.wrapperKurslar} container`}>
                <span className={style.headrTotle}>Our courses</span>
                <h3>Choose the right course for you</h3>
                <div className={style.buttons}>
                    <button className={open === 1 && style.buttonActive} onClick={() => clicked(1)}>IELTS</button>
                    <button className={open === 2 && style.buttonActive} onClick={() => clicked(2)}>DTM</button>
                </div>
                <div className={style.cards}>
                    {
                        data.map((item, index) =>
                            <div key={index} className={style.cardSub} onClick={(e) => console.log(e.target)}>
                                <img src={item?.bigIcon} className={style.cardImg} alt="noimg"/>
                                <h4>{item?.bigTitle}</h4>
                                <div className={style.cardbodyText}>
                                    <img src={item.timeIcon} alt=""/>
                                    <span>Davomiyligi: <b>{item.timeTitle} oy</b></span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={item.groupIcon} alt=""/>
                                    <span>{item.groupTitle}</span>
                                </div>
                                <div className={style.cardbodyText}>
                                    <img src={item.infoIcon} alt=""/>
                                    <span>{item.infoTitle}</span>
                                </div>
                                <button className={style.buttonhover}>
                                    Kursga yozilish
                                    <img src={str} alt=""/>
                                </button>
                            </div>
                        )
                    }
                </div>

            </div>
        </div>

    );
};

export default Index;