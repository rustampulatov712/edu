import raketa from "../../assets/img/Rocket.svg";
import clock from "../../assets/img/soat.svg";
import contact from "../../assets/img/contact.svg";
import warring from "../../assets/img/warring.svg";
import present from "../../assets/img/Presentation - Line.svg";
import book from "../../assets/img/Swatchbook.svg";
import dash from "../../assets/img/Dashboard.svg";



export const data = [
    {
        id: 1,
        bigIcon: raketa,
        bigTitle: "General English",
        timeIcon: clock,
        timeTitle: '4',
        groupIcon: contact,
        groupTitle: "10-12 kishilik guruh",
        infoIcon: warring,
        infoTitle: 'Minimal bosqich: noldan yoki undan yuqori'
    },
    {
        id: 2,
        bigIcon: present,
        bigTitle: "Intermedite",
        timeIcon: clock,
        timeTitle: '4',
        groupIcon: contact,
        groupTitle: "10-12 kishilik guruh",
        infoIcon: warring,
        infoTitle: 'Minimal bosqich: noldan yoki undan yuqori'
    },
    {
        id: 3,
        bigIcon: book,
        bigTitle: "IELTS focus",
        timeIcon: clock,
        timeTitle: '4',
        groupIcon: contact,
        groupTitle: "10-12 kishilik guruh",
        infoIcon: warring,
        infoTitle: 'Minimal bosqich: noldan yoki undan yuqori'
    },
    {
        id: 4,
        bigIcon: dash,
        bigTitle: "Fast IELTS",
        timeIcon: clock,
        timeTitle: '4',
        groupIcon: contact,
        groupTitle: "10-12 kishilik guruh",
        infoIcon: warring,
        infoTitle: 'Minimal bosqich: noldan yoki undan yuqori'
    },
]