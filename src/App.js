import './App.css';
import Dashbord from "./views/Dashbord";
import 'bootstrap/dist/css/bootstrap.css'
import Lesson from "./views/Lesson";
import Team from "./views/Team";
import Result from "./views/Result";
import News from "./views/News";
import Blog from "./views/Blog";
import Sps from "./views/SPC"
import Info from "./views/Info";
import Test from "./views/test";
import Footer from "./views/Footer";

function App() {
    return (
        <div>
            <script src="//code.jivosite.com/widget/v8NDz3sg52" async></script>
            <Dashbord/>
            <Lesson/>
            <Team/>
            <Result/>
            <News/>
            <Blog/>
            <Sps/>
            <Info/>
            <Footer/>
            {/*<Test/>*/}
        </div>
    );
}

export default App;
